<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tutorijal' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ko6pWtw>j/tZ?bW-(Kt>kn1RKrAX/<}c#a8MSkMEjc,.BNQ, qhe>hMElFRD{<AS' );
define( 'SECURE_AUTH_KEY',  '9cfWG*szMC65Ko_I%-`h_l{aTQkZ9~BgH)YB=aFp-}|u4b^IVW@.baR!e>1vj-&@' );
define( 'LOGGED_IN_KEY',    '_4=h9jb:6,,Gya13M=AmsOZU/#Z/h5/cG_kj?Q#$H>t>msorxRly_368dLB]Yu!z' );
define( 'NONCE_KEY',        '-;<#^KpgKIvG.{iir}/32ZuW$a[}b5d~RRfYlqeqS9@X>HzkMH6f&dH5|>&>xVy;' );
define( 'AUTH_SALT',        'I}:AnW4#i)F0!5rPf4Oqi}$wxlu5%V#P9pXWI0 Z+t,FW4SQ<PNH6f.c7F9jbJ5I' );
define( 'SECURE_AUTH_SALT', 'y%}<-d l`aGj!-;o~<)nQ&@mE3M+~7RF^qm_r^~{#t9oO5Oh=o5xl <=qSxK*ud<' );
define( 'LOGGED_IN_SALT',   'Q_7c{~wDX5V5kth-$N!eJP-}>c_YGUt.M}ATV L2b$2F3:UUcVb*?uZwJQ8^.=?l' );
define( 'NONCE_SALT',       'l8g7-O;ZCOY5s^=gP6EnY&12HdzZfJ=OL-PP+bdBBIqnf]DcFZR7Hi3x-XU%q7CB' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
