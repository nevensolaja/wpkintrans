<?php

    /* Template Name:HOMEPAGE */

?>
<html>
     <head>
          <title>Kin Trans</title>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.1">
          <link rel="stylesheet" href="css/style.css" type="text/css">
          <link rel="stylesheet" href="css/Raleway-Regular.ttf">
     </head>
     <body>
         <div class="header">
             <div class="header-top">

                    <!-- Header top start -->
                    <div class="header-top-up">
                        <div class="slope-one"></div>
                        <div class="header-top-first">
                            <img src="images/time-8-32.png"/>
                            <p class="time">Radno vreme:pon-pet od 07:00 - 18:00</p>
                        </div>
                        <div class="header-top-secnd">
                            <li>
                                <a href="#"><img src="images/facebook-32.png"/></a>
                            </li>
                            <li>   
                                <a href="#"><img src="images/twitter-32.png"/></a>
                            </li> 
                            <li>   
                                <a href="#"><img src="images/linkedin-3-32.png"/></a>
                            </li>
                            <li>   
                                <a href="#"><img src="images/google-plus-4-32.png"/></a>
                            </li>   
                        </div>
                    </div> 
                    <!-- Header top end -->
                    <!--Header top middle start-->
                    <div class="header-top-middle">
                        <div class="container">
                            <div class="header-name">
                                <h3>Kin Trans</h3>
                                <p>Kvalitetan prevoz za vas</p>
                            </div>
                            <div class="header-contact">
                                <div class="header-call">
                                    <img src="images/phone-62-32.png"/>
                                    <div class="header-call-us">
                                        <p class="call">Pozovite Nas</p>
                                        <p class="number">+381 64 23 54 608</p>
                                    </div>
                                </div>
                                <div class="header-mail">
                                    <img src="images/forward-32.png"/>
                                    <div class="header-send-us-mail">
                                        <p class="send">Posaljite nam mail</p>
                                        <p class="mail">kintrans@gmail.com</p>
                                    </div>
                                </div>
                                <div class="enquiry">
                                    <p>ZAHTEV ZA UPIT</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Header top middle and-->
                    <div class="header-top-downe">
                        <div class="slope-two"></div>
                        <ul>
                            <li><a href="#">Pocetna</a></li>
                            <li><a href="#">O Nama</a> </li>
                            <li><a href="#">Usluge</a></li>
                            <li><a href="#">Galerija</a></li>
                            <li><a href="#">Kontakt</a></li>
                        </ul>
                           
                    </div>
                 
             </div>
           




         </div>

         <div class="banner">
            <div class="container">
                <div class="content">
                    <div class="name">
                        <p class="nam-bor"></p>
                        <h1>NAJKVALITETNIJI<br> 
                            PREVOZ<br>
                            ROBE</h1>
                    </div>
                    <div class="description">
                        <p>Prevoz robe u domacem i medjunarodnom saobracaju.Savremena vozila za prevoz tereta [...]</p>
                    </div>
                </div>   
            </div>    
         </div>

         <div class="service" data-stella-backgground-ratio="0.4">
             <div class="container">
                <div class="ser-vice">

                    <div class="ser">
                        <div class="ser-img one">
                            <div class="read-more">
                                <div class="r-m">
                                    <a href="#"><p>OPSIRNIJE</p></a>
                                </div>   
                                <div class="arrow">
                                    <a href="#"><img src="images/arrow.png"/></a>
                                </div>
                            </div>
                        </div>   
                        <div class="ser-text">   
                            <h3>Transport robe 
                                sleperima</h3>
                            <p class="line"></p>
                            <p>We have a wide experience in overland industry specific logistic solutions like pharmaceutical logistics, retail and automotive logistics by train or road.
                            </p>
                        </div>
                    </div>

                    <div class="ser">
                        <div class="ser-img two">
                            <div class="read-more">
                                <div class="r-m">
                                    <a href="#"><p>OPSIRNIJE</p></a>
                                </div>    
                                <div class="arrow">
                                    <a href="#"><img src="images/arrow.png"/></a>
                                </div>
                            </div>
                        </div>   
                        <div class="ser-text">   
                        <h3>Transport robe 
                            sleperima</h3>
                        <p class="line"></p>
                        <p>We have a wide experience in overland industry specific logistic solutions like pharmaceutical logistics, retail and automotive logistics by train or road.</p>
                        </div>
                    </div>
                    <div class="ser">
                        <div class="ser-img three">
                            <div class="read-more">
                                <div class="r-m">
                                    <a href="#"><p>OPSIRNIJE</p></a>
                                </div>  
                                <div class="arrow">
                                    <a href="#"><img src="images/arrow.png"/></a>
                                </div>
                            </div>
                        </div>   
                        <div class="ser-text">   
                        <h3>Transport robe 
                            sleperima</h3>
                        <p class="line"></p>
                        <p>We have a wide experience in overland industry specific logistic solutions like pharmaceutical logistics, retail and automotive logistics by train or road.</p>
                        </div>
                    </div>
                </div>  
                <div class="details">
                    <p>VIDI DETALJE</p>
                </div>  
             </div>    
         </div>
         <div class="offer">
              <div class="container">
                    <div class="whatweoffer">
                          <h3>Nasa Ponuda Usluga</h3>
                    </div>
                    <div class="what-we-offer">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="off-img">
                                        <img src="images/tra.png"/>
                                    </div>
                                    <div class="off-tex">
                                        <h3>PREVOZ POLJOPRIVREDNE MEHANIZACIJE</h3>
                                        <p class="">Need custom logistic service? We got it covered. From overland, air, rail and sea transportation. Fast, safe and accurate shipment provided all over the globe.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="off-img">
                                        <img src="images/seeder.png"/>
                                    </div>
                                    <div class="off-tex">
                                        <h3>PREVOZ PRIKLJUCNI MASINA</h3>
                                        <p class="">Need custom logistic service? We got it covered. From overland, air, rail and sea transportation. Fast, safe and accurate shipment provided all over the globe.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="off-img">
                                        <img src="images/512.png"/>
                                    </div>
                                    <div class="off-tex">
                                        <h3>PREVOZ CISTERNI</h3>
                                        <p class="">Need custom logistic service? We got it covered. From overland, air, rail and sea transportation. Fast, safe and accurate shipment provided all over the globe.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="off-img">
                                        <img src="images/excavator.png"/>
                                    </div>
                                    <div class="off-tex">
                                        <h3>PREVOZ GRADJEVINSKIH MASINA</h3>
                                        <p class="">Need custom logistic service? We got it covered. From overland, air, rail and sea transportation. Fast, safe and accurate shipment provided all over the globe.</p>
                                    </div>
                                </div>
                            </div>
                    </div>  
              </div>            
         </div>
         <div class="industry">
                    <div class="goods">
                        <div class="empty"></div>
                        <div class="trans-goods">
                                <div class="sector-sec">
                                    <p class="bor"></p>
                                    <h3>PRIVREDNI SEKTORI KOJE POKRIVAMO</h3>
                                    <p>Mi pokrivamo različite sektore industrije od poljoprivredni proizvoda ,hemijskih, hrane i pića trajnih dobara i više.</p>
                                </div>
                                <div class="sector one">
                                    <div class="sector-img">
                                        <img src="images/package.png"/>
                                    </div>
                                    <div class="sector-text">
                                        <p>Pakovana Roba</p>
                                    </div>
                                </div>
                                <div class="sector two">
                                    <div class="sector-img">
                                        <img src="images/oil-barrel.png"/>
                                    </div>
                                    <div class="sector-text">
                                        <p>Naft, Ulje i Gas</p>
                                    </div>
                                </div>
                                <div class="sector three">
                                    <div class="sector-img">
                                        <img src="images/sugar-beet.png"/>
                                    </div>
                                    <div class="sector-text">
                                        <p>Secerna repa</p>
                                    </div>
                                </div>
                                <div class="sector four">
                                    <div class="sector-img">
                                        <img src="images/sunflower.png"/>
                                    </div>
                                    <div class="sector-text">
                                        <p>Suncokret</p>
                                    </div>
                                </div>
                                <div class="sector five">
                                    <div class="sector-img">
                                        <img src="images/wheat.png"/>
                                    </div>
                                    <div class="sector-text">
                                        <p>Psenica</p>
                                    </div>
                                </div>
                        </div>
                    </div>    
                <div class="truck">

                </div>





         </div>
         <div class="element">
             <div class="container">
                 <div class="ele first">
                     <div class="ele-img">
                         <img src="images/cur.jpg"/>
                     </div>
                     <div class="ele-head">
                         <h3>pracenje</h3>
                     </div>
                     <div class="ele-pas">
                         <p>Need custom logistic service? We got it covered. From overland, air, rail and sea transportation. Fast, safe and accurate shipment provided all over the globe.</p>
                     </div>
                 </div>
                 <div class="ele first">
                    <div class="ele-img">
                        <img src="images/travel.png"/>
                    </div>
                    <div class="ele-head">
                        <h3>podrska</h3>
                    </div>
                    <div class="ele-pas">
                        <p>Need custom logistic service? We got it covered. From overland, air, rail and sea transportation. Fast, safe and accurate shipment provided all over the globe.</p>
                    </div>
                </div>
                <div class="ele first">
                    <div class="ele-img">
                        <img src="images/copyright.png"/>
                    </div>
                    <div class="ele-head">
                        <h3>isporuka</h3>
                    </div>
                    <div class="ele-pas">
                        <p>Need custom logistic service? We got it covered. From overland, air, rail and sea transportation. Fast, safe and accurate shipment provided all over the globe.</p>
                    </div>
                </div>
             </div>
         </div>
         <div class="benefit">
             <div class="container"> 
                <div class="provide">
                    <p class="about">O NAMA</p>
                    <h2>KAMIONSKI PREVOZ</h2>
                    <p class="des">Prevoz robe u drumskom saobraćaju je naša osnovna delatnost. Bavimo se prevozom poljoprivredne mehanizacije i opreme, ali i prevozom građevinskih mašina i materijala. Pružamo usluge unutrašnjeg prevoza bilo kakvog tereta do 13 tona, gabarita: 7.3m dužine, 2.5m širine i 3m visine. Takođe se bavimo uslugama selidbi,kako na teritoriji Novog Sada, tako i širom republike Srbije. Posedujemo sve dozvole i sertifikate.
                        Sa ponosom možemo da kažemo da za sve ove godine obavljanja ove delatnosti smo izvršili veliki broj uspešno završenih prevoza.

                        Važno nam je da znate, da svakom poslu pristupamo veoma profesionalno i da na više načina obezbeđujemo tovar, kako se tokom transporta ne bi oštetio. Na prioritetnom mestu su nam sigurnost tereta i zadovoljan klijent.
                    </p>
                    <div class="book">
                        <a href="#"><p>SAZNAJTE VISE</p></a>
                    </div>
                </div>
                <div class="several">
                    <div class="t-t">
                        <div class="three-theard">
                            <img src="images/cargo2.png"/>
                            <p>Bezbednost prevoza robe</p>
                        </div>
                        <div class="three-theard">
                            <img src="images/cargo4.png"/>
                            <p>Merenje tezine kod utovara i istovara</p>
                        </div>
                        <div class="three-theard">
                            <img src="images/cargo1.png"/>
                            <p>Obezbedjena logistika</p>
                        </div>
                        <div class="three-theard">
                            <img src="images/cargo.png"/>
                            <p>Sigurnost pakovanja</p>
                        </div>
                        <div class="three-theard">
                            <img src="images/declaration.png"/>
                            <p>Urednost pratece dokumentacije</p>
                        </div>
                        <div class="three-theard">
                            <img src="images/declaration1.png"/>
                            <p>Osiguranje CMR-ova</p>
                        </div>
                        <div class="three-theard">
                            <img src="images/declaration2.png"/>
                            <p>Zastita korisnikovih prava</p>
                        </div>
                        <div class="three-theard">
                            <img src="images/declaration3.png"/>
                            <p>Bezbednost prevoza<br>
                                lako lomljive robe</p>
                        </div>
                        <div class="three-theard">
                            <img src="images/declaration4.png"/>
                            <p>Licenca za prevoz</p>
                        </div>
                    </div>    
                </div>
             </div>   
         </div>
         <div class="client">
            <div class="container">
               <div class="news">
                   <h2>Contract logistics</h2>
                   <p class="lin"></p>
                   <p> From overland, air, rail and sea transportation. Fast, safe and accurate shipment provided all over the globe.</p>
               </div>
               <div class="latest">
                   <div class="lat">
                       <div class="one">
                           <img src="images/field.png"/>
                       </div>
                       <div class="one-text">
                           <h3>Contract logistics</h3>
                           <p class="d">Need custom logistic service? We got it covered. From overland, air, rail and sea transportation. Fast, safe and accurate shipment provided all over the globe.</p>
                           <p class="p">Contract logistics</p>
                       </div>
                   </div>
                   <div class="lat">
                        <div class="one">
                            <img src="images/truck-navigation.png"/>
                        </div>
                        <div class="one-text">
                            <h3>Contract logistics</h3>
                            <p class="d">Need custom logistic service? We got it covered. From overland, air, rail and sea transportation. Fast, safe and accurate shipment provided all over the globe.</p>
                            <p class="p">Contract logistics</p>
                        </div>
                    </div>
                    <div class="lat">
                        <div class="one">
                            <img src="images/laptop.jpg"/>
                        </div>
                        <div class="one-text">
                            <h3>Contract logistics</h3>
                            <p class="d">Need custom logistic service? We got it covered. From overland, air, rail and sea transportation. Fast, safe and accurate shipment provided all over the globe.</p>
                            <p class="p">Contract logistics</p>
                        </div>
                    </div>
               </div>
               
            </div>  
         </div>
         <div class="slideshow-container">
             <div class="mySlides">
                
                 <div class="t">
                     <p class="what">STA SU KLIJENTI REKLI O NAMA</p>
                     <p class="li"></p>
                     <div class="quote">
                        <img src="images/quotes-32.png"/>
                     </div>   
                     <p class="say">Transit WordPress Theme is a 100% responsive to all kinds of screens from high resolutions to mobile and tablets and is ready for retina display. It can be installed with just one click and is easily customizable, thus making it is easier to build websites. Our themes are SEO optimized based on best SEO practices, thus ranking it high on search engines.</p>
                     <p class="nam">Ana Vuckovic</p>
                     <p class="company-name">Panonija Prevoz</p>
                 </div>
             </div>
             <div class="mySlides">
                
                <div class="t">
                     <p class="what">STA SU KLIJENTI REKLI O NAMA</p>
                     <p class="li"></p>
                     <div class="quote">
                        <img src="images/quotes-32.png"/>
                     </div>
                     <p class="say">Transit WordPress Theme has a lot of shortcodes that can be used to set up fancier content easily throughout the website. This theme is flexible enough to easily override default template file using a child theme. Restaurant WordPress Theme is designed for high performance and is light-weighted.</p>
                     <p class="nam">Margko Markovic</p>
                     <p class="company-name">Formula Transport</p>
                </div>
             </div>
             <div class="mySlides">
                
                <div class="t">
                     <p class="what">STA SU KLIJENTI REKLI O NAMA</p>
                     <p class="li"></p>
                     <div class="quote">
                        <img src="images/quotes-32.png"/>
                     </div>
                     <p class="say">Transit WordPress Theme is a 100% responsive to all kinds of screens from high resolutions to mobile and tablets and is ready for retina display. It can be installed with just one click and is easily customizable, thus making it is easier to build websites. Our themes are SEO optimized based on best SEO practices, thus ranking it high on search engines.</p>
                     <p class="nam">Petar Petrovic</p>
                     <p class="company-name">ABM Transport</p>
                </div>
             </div>
             <div class="dot-container">
                <span class="dot" onclick="currentSlide(1)"></span>
                <span class="dot" onclick="currentSlide(2)"></span>
                <span class="dot" onclick="currentSlide(3)"></span>
             </div>
             <script>
                var slideIndex = 1;
                showSlides(slideIndex);
                
                function plusSlides(n) {
                  showSlides(slideIndex += n);
                }
                
                function currentSlide(n) {
                  showSlides(slideIndex = n);
                }
                
                function showSlides(n) {
                  var i;
                  var slides = document.getElementsByClassName("mySlides");
                  var dots = document.getElementsByClassName("dot");
                  if (n > slides.length) {slideIndex = 1}    
                  if (n < 1) {slideIndex = slides.length}
                  for (i = 0; i < slides.length; i++) {
                      slides[i].style.display = "none";  
                  }
                  for (i = 0; i < dots.length; i++) {
                      dots[i].className = dots[i].className.replace(" active", "");
                  }
                  slides[slideIndex-1].style.display = "block";  
                  dots[slideIndex-1].className += " active";
                }
                </script>
         </div>
         <div class="footer">
             <div class="container">
                 <div class="cols">
                     <div class="column one">
                         <a href="#"><img src="images/footer-log.png"/></a>
                         <p>Aenean sit amet leo scelerisqu consectetur hendrerit. Sed facilisis at sapien sedrutrum. In finibus ex nec odio egestas, vel auct urna varius. Aenean tempor varius diam, gravida ligula. Aenean suscipit, justo nec b gravida, sem mi interdum elit, vitae suscipit n lacus sed felis. Etiam dolor erat, cursus</p>
                         <div class="social">
                             <div class="soc face">
                                 <a href="#"><img src="images/facebook-32.png"/></a>
                             </div>
                             <div class="soc twit">
                                <a href="#"><img src="images/twitter-32.png"/></a>
                            </div>
                            <div class="soc inst">
                                <a href="#"><img src="images/linkedin-3-32.png"/></a>
                            </div>
                            <div class="soc googl">
                                <a href="#"><img src="images/google-plus-4-32.png"/></a>
                            </div>
                         </div>
                     </div>
                     <div class="column two">
                            <p class="ourservice">Nase Usluge</p>
                            <div class="ours 1">
                                <img src="images/arrow.png"/>
                                <a href="#"><p>Prevoz pakovane robe</p></a>
                            </div>
                            <div class="ours 2">
                                <img src="images/arrow.png"/>
                                <a href="#"><p>Prevoz zitarica</p></a>
                            </div>
                            <div class="ours 3">
                                <img src="images/arrow.png"/>
                                <a href="#"><p>Prevoz gradjevinskog materijala</p></a>
                            </div>
                            <div class="ours 4">
                                <img src="images/arrow.png"/>
                                <a href="#"><p>Prevoz cisterni i industrijskih buradi</p></a>
                            </div>
                            <div class="ours 5">
                                <img src="images/arrow.png"/>
                                <a href="#"><p>Prevoz prikljucnih masina</p></a>
                            </div>
                            <div class="ours 6">
                                <img src="images/arrow.png"/>
                                <a href="#"><p>Prevoz ostale robe</p></a>
                            </div>
                    </div>
                    <div class="column three">
                        <p class="worktime">Radno Vreme</p>
                        <div class="week">
                            <div class="day">
                                <p>Ponedeljak</p>
                            </div>
                            <div class="time">
                                <p>7:00 - 18:00</p>
                            </div>
                        </div>
                        <div class="week">
                            <div class="day">
                                <p>Utorak</p>
                            </div>
                            <div class="time">
                                <p>7:00 - 18:00</p>
                            </div>
                        </div>
                        <div class="week">
                            <div class="day">
                                <p>Sreda</p>
                            </div>
                            <div class="time">
                                <p>7:00 - 18:00</p>
                            </div>
                        </div>
                        <div class="week">
                            <div class="day">
                                <p>Cetvrtak</p>
                            </div>
                            <div class="time">
                                <p>7:00 - 18:00</p>
                            </div>
                        </div>
                        <div class="week">
                            <div class="day">
                                <p>Petak</p>
                            </div>
                            <div class="time">
                                <p>7:00 - 18:00</p>
                            </div>
                        </div>
                        <div class="week">
                            <div class="day">
                                <p>Subota</p>
                            </div>
                            <div class="time">
                                <p>7:00 - 15:30</p>
                            </div>
                        </div>
                    </div>
                    <div class="column four">
                        <p class="contact">Kontaktirajte Nas</p>
                        <div class="cont 1">
                            <img src="images/arrow.png"/>
                            <a href="#"><p> Fusce nulla tellus, sodales ultricies dictum eget</p></a>
                        </div>
                        <div class="cont 2">
                            <img src="images/phone-62-16.png"/>
                            <a href="#"><p>021 6411-574</p></a>
                        </div>
                        <div class="cont 3">
                            <img src="images/mobile-phone-8-16.png"/>
                            <a href="#"><p>064 2354-608</p></a>
                        </div>
                        <div class="cont 4">
                            <img src="images/new-post-16.png"/>
                            <a href="#"><p>kintrans@gmail.com</p></a>
                        </div>
                      
                </div>
                 </div>
                 
             </div>
         </div>
    </body>
</html>